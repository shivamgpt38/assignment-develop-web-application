### How to run

1. **Open your terminal and navigate to where you'd like to store the project**

2. **Clone the repo using:** 
`git clone https://bitbucket.org/shivamgpt38/assignment-develop-web-application.git`

3. **cd into your newly created repo:** 
`cd assignment-develop-web-application`

4. **Run npm install to install all the node package dependencies:** 
`npm install`

5. **Change the MONGO DB URL in config.js file**

6. **After installation run:**
`npm start`

### API Routes to hit
    
* /api/sign-up    (for user signup)   
* /api/login    (to get token)        
* /api/home    (to check auth)
* /find/:email    (to find user)


### Steps screen shot

1- Go to localhost:9999
![Alt text](https://i.ibb.co/HGd0XN2/Screen-Shot-2019-12-23-at-2-59-07-PM.png)

2- Login with your details Use Sign up api to create account
![Alt text](https://i.ibb.co/4gkxMJB/Screen-Shot-2019-12-23-at-3-05-31-PM.png)

3- After login Find user by email address (you need to create 2 account to communicate with each other for POC)
![Alt text](https://i.ibb.co/fSk6fBh/Screen-Shot-2019-12-23-at-2-59-51-PM.png)

4- Click on chat icon following using username of the user
![Alt text](https://i.ibb.co/RP9f7r1/Screen-Shot-2019-12-23-at-3-00-12-PM.png)

5- A window will open on both end (sender and recevier)
![Alt text](https://i.ibb.co/JQLX4yh/Screen-Shot-2019-12-23-at-3-00-20-PM.png)

6- start chating 
![Alt text](https://i.ibb.co/dmtNMsg/Screen-Shot-2019-12-23-at-3-00-58-PM.png)




