$(document).ready((e) => {
    var socket = io.connect('http://localhost:8888');
    let room = "";
    let token = localStorage.getItem('token');
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/api/home",
        "method": "GET",
        "headers": {
            "Content-Type": "application/json",
            "Authorization": token
        }
    }

    $.ajax(settings).done(function (response) {
        if (response.msg === "invalid token") {
            Materialize.toast('Invalid login', 4000)
            document.location.href = '/'
        } else {
            console.log(response);
            localStorage.setItem('loginedWith', response.data.user.username)
        }
    });


    $('#findBtn').on('click', (evnt) => {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://localhost:9999/api/find/" + $("#icon_prefix").val(),
            "method": "GET",
            "headers": {
                "Authorization": token
            }
        }

        $.ajax(settings).done(function (resp) {
            console.log(resp);
            $('#userlist').show();
            $('#usertable').show();

            resp.data.map(user => {
                let userTable = "<tr>";
                userTable += "<td>" + user.email + "</td>";
                userTable += "<td>" + user.username + "</td>";
                userTable += '<td><a id="chatboxbutn" href="#"> <i class="material-icons ' + user.username + '">chat</i></a></td>';
                $('#usertable').append(userTable);
                userTable = "";
            })
        });
    })

    $(document).on('click','#chatboxbutn',(ev)=>{
        console.log(ev)
        $('#chatbox').show();
        console.log("send msg to = " +ev.target.classList[1])
        room  = localStorage.getItem('loginedWith')+'-'+ev.target.classList[1]
        socket.emit('join', room);
    })

    $(window).keyup(function (event) {
        if (event.keyCode == 13) {
            let ms = {
                user:localStorage.getItem('loginedWith'),
                msg:$('#msg').val()
            };
            socket.emit("send message",ms)
            $('#msg').val('')
        }
    })

    socket.on('new_chat', function (data) {
        socket.emit('joinme', data);
        $('#chatbox').show();
    });

    socket.on('new_msg', function (data) {
        $('#chatHistory').append(data + "<br>")
    });
})