const express = require('express');
const app = express();
const config = require('./config.js');
const morgan = require('morgan');
var http = require('http').createServer(app);
const db = require('./db/db');
var io = require('socket.io')(http);
var path = require('path');
//mongodb connect
db(config.db);

app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'ejs');

// set path for static assets
var assetsPath = path.join(__dirname, '/public');
app.use(express.static(assetsPath));
//morgan middleware
app.use(morgan('tiny'));
app.set('socketio', io);
//express default parser middleware
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//exposed ports
const port = process.env.PORT || 9999;

//routes
const auth = require('./routes/user');
const index = require('./routes/index');
app.use('/api', auth);
app.use('/', index);


io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('join', function (data) {
        socket.join(data);
        console.log('a user connected to - '+data)
        io.emit('new_chat',data)
    });

    socket.on('joinme',(data)=>{
        socket.join(data);
    })
    socket.on('send message',(data)=>{
        let msg = "["+new Date().toTimeString().split(' ')[0]+"] | "+ data.msg +" - "+data.user
        console.log(msg)
        io.emit('new_msg',msg)
    })

    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});


app.listen(port, () => {
    console.log('Api listening on :' + port)
})

http.listen(8888, function () {
    console.log('Socket listening on :' + 8888);
});
