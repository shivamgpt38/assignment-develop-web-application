const express = require('express');
const userModel = require('./../models/usermodel');
const msgModal = require('./../models/msg');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('./../config');
module.exports = (function () {
    'use strict';
    const router = express.Router();

    router.post('/sign-up', (req, res) => {
        // data sample
        // {
        //     "username":"shivam2",
        //     "password":"shivam",
        //     "email":"shivam@agatsa.com"
        // }
        if (req.body.email &&
            req.body.username &&
            req.body.password) {
            userModel.find({ username: req.body.username }, (e, d) => {
                if (e) {
                    return e;
                } else {
                    if (d.length > 0) {
                        return res.status(409).json({ msg: "duplicate key:username" })
                    } else {
                        userModel.find({ email: req.body.email }, (ee, dd) => {
                            if (e) {
                                return e;
                            }
                            else {
                                if (dd.length > 0) {
                                    return res.status(409).json({ msg: "duplicate key:email" })
                                } else {
                                    let user = new userModel({
                                        username: req.body.username,
                                        email: req.body.email
                                    });
                                    bcrypt.genSalt(10, (err, salt) => {
                                        bcrypt.hash(req.body.password, salt, (err, hash) => {
                                            user.password = hash;
                                            console.log(user);
                                            user.save((err) => {
                                                if (err) return console.log(err);
                                                res.json({ msg: 'created' });
                                            });
                                        })
                                    })
                                }
                            }
                        })

                    }
                }
            })
        }
    });
    router.post('/login', (req, res) => {
        if (req.body.username &&
            req.body.password) {
            let query = { username: req.body.username };
            userModel.findOne(query, (err, user) => {
                if (err) return console.log(err);
                bcrypt.compare(req.body.password, user.password, (err, decode) => {
                    if (decode === true) {
                        let token = jwt.sign({ user }, config.secret, {
                            expiresIn: 10000
                        });
                        res.json({
                            msg: 'welcome',
                            token: token
                        });
                    }
                })
            })
        }
    });
    router.get('/home', (req, res) => {
        if (req.headers.authorization) {
            jwt.verify(req.headers.authorization, config.secret, (err, decode) => {
                if (err) return res.json({ msg: 'invalid token' });
                res.json({ data: decode });
            })
        } else {
            return res.status(403).json({ msg: 'no token provided' });
        }
    })
    router.get('/find/:email', (req, res) => {
        if (req.headers.authorization) {
            jwt.verify(req.headers.authorization, config.secret, (err, decode) => {
                if (err) return res.json({ msg: 'invalid token' });

                let query = { email: req.params.email };
                userModel.find(query, (err, users) => {
                    if (err) {
                        return err;
                    }
                    else {
                        let userList = []
                        users.map(user => {
                            userList.push({ "username": user.username, "email": user.email })
                        })
                        return res.status(200).json({ data: userList })
                    }
                })

            })
        } else {
            return res.status(403).json({ msg: 'no token provided' });
        }

    })


    return router;
})();