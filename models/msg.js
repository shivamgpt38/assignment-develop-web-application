const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const msgSchema = new Schema({
    users:{
        type:String,
        required:true
    },
    history:[{
        date:{
            type:String
        },
        msg:{
            type:String
        }
    }]
})

module.exports = mongoose.model('msg',msgSchema);